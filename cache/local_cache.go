// @Author EthanScriptOn
// @Desc
package cache

import (
	"gitee.com/fatzeng/srf_switch_basic_components/util"
	"sync"
	"time"
)

type SrfSwitchLocalCache struct {
	isOpenCache          bool
	mu                   *sync.RWMutex
	cacheContainer       map[interface{}]interface{}
	cacheTimingContainer map[interface{}]*time.Timer
}

func (s *SrfSwitchLocalCache) IsEnabled() bool {
	return s.isOpenCache
}

func (s *SrfSwitchLocalCache) SetEnabled(enabled bool) {
	s.isOpenCache = enabled
}

func (s *SrfSwitchLocalCache) Add(key, value interface{}, expiration time.Duration) error {
	return util.LockWithAsync(s.mu.Lock, s.mu.Unlock, func() (err error) {
		s.cacheContainer[key] = value
		s.cacheTimingContainer[key] = time.AfterFunc(expiration, func() {
			delete(s.cacheContainer, key)
			delete(s.cacheTimingContainer, key)
		})
		return
	})
}

func (s *SrfSwitchLocalCache) Get(key interface{}) (result interface{}, canFound bool) {
	if err := util.LockWithAsync(s.mu.RLock, s.mu.RUnlock, func() (err error) {
		result, canFound = s.cacheContainer[key]
		return
	}); err != nil {
		return nil, false
	}
	return
}

func (s *SrfSwitchLocalCache) Delete(key interface{}) {
	_ = util.LockWithAsync(s.mu.Lock, s.mu.Unlock, func() (err error) {
		delete(s.cacheContainer, key)
		delete(s.cacheTimingContainer, key)
		return
	})
}

func (s *SrfSwitchLocalCache) Clear() {
	_ = util.LockWithAsync(s.mu.Lock, s.mu.Unlock, func() (err error) {
		s.cacheTimingContainer = make(map[interface{}]*time.Timer)
		s.cacheContainer = make(map[interface{}]interface{})
		return
	})
}

func (s *SrfSwitchLocalCache) Source() interface{} {
	return nil
}

func (s *SrfSwitchLocalCache) Update(key interface{}, newValue interface{}, newExpiration time.Duration) error {
	return s.Add(key, newValue, newExpiration)
}

func (s *SrfSwitchLocalCache) Touch(key interface{}, newExpiration time.Duration) error {
	return util.LockWithAsync(s.mu.Lock, s.mu.Unlock, func() (err error) {
		if timer, ok := s.cacheTimingContainer[key]; ok {
			timer.Reset(newExpiration)
		}
		return
	})
}
