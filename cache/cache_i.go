// @Author EthanScriptOn
// @Desc
package cache

import "time"

type Cache interface {
	// IsEnabled 检查缓存是否启用
	IsEnabled() bool

	// Set 开启或关闭缓存
	SetEnabled(enabled bool)

	// Add 添加缓存项
	Add(key, value interface{}, expiration time.Duration) error

	// Get 获取缓存项
	Get(key interface{}) (interface{}, bool)

	// Delete 删除缓存项
	Delete(key interface{})

	// Clear 清空所有缓存
	Clear()

	// Source 获取缓存写入源信息（根据需求定义具体结构）
	Source() interface{}

	// Update 更新缓存条目
	Update(key interface{}, newValue interface{}, newExpiration time.Duration) error

	// Touch 更新缓存条目的过期时间，而不改变其值
	Touch(key interface{}, newExpiration time.Duration) error
}
