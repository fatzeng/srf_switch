// @Author EthanScriptOn
// @Desc
package config

var SrfSwitchGlobalConfig = new(SrfSwitchConfig)

type SrfSwitchConfig struct {
	SrfLogConfig         []*SrfLogConfig    `tag:"srf-switch_log"`
	SrfEnvConfig         SrfEnvConfig       `tag:"srf-env"`
	SrfCoreOptionsConfig SrfCoreOptions     `tag:"srf-core-options"`
	SrfLocatorConfig     SrfLocatorConfig   `tag:"srf-locate"`
	SrfBridgeConfig      []*SrfBridgeConfig `tag:"srf-bridge"`
}

type SrfLogFilter struct {
	FilterTag    string `yaml:"filter_tag" prop:"filter_tag" ini:"filter_tag" flag:"filter_tag"`
	FilterEnable bool   `yaml:"filter_enable" prop:"filter_enable" ini:"filter_enable" flag:"filter_enable"`
}

type SrfLogConfig struct {
	Level    string                `yaml:"level" prop:"level" ini:"level" flag:"level"`
	Filter   []*SrfLogFilter       `yaml:"filter" prop:"filter" ini:"filter" flag:"filter"`
	Template string                `yaml:"template" prop:"template" ini:"template" flag:"template"`
	Output   []*SrfLogOutputConfig `yaml:"output" prop:"output" ini:"output" flag:"output"`
}

type SrfLogOutputConfig struct {
	OutputType string `yaml:"output_type" prop:"output_type" ini:"output_type" flag:"output_type"`
	OutputPath string `yaml:"output_path" prop:"output_path" ini:"output_path" flag:"output_path"`
}

type SrfEnvConfig struct {
	SrfEnv string `yaml:"env" prop:"env" ini:"env" flag:"env"`
}

type SrfCoreOptions struct {
	SrfEnableCache         bool   `yaml:"enable-cache" prop:"enable-cache" ini:"enable-cache" flag:"enable-cache"`
	SrfStatisticSwitchName string `yaml:"statistic-switch-name" prop:"statistic-switch-name" ini:"statistic-switch-name" flag:"statistic-switch-name"`
}

type SrfLocatorConfig struct {
	SrfNamespace string `yaml:"namespace" prop:"namespace" ini:"namespace" flag:"namespace"`
	SrfGroup     string `yaml:"group" prop:"group" ini:"group" flag:"group"`
}

type SrfBridgeConfig struct {
	SrfDsUniqueness string `yaml:"ds-uniqueness" prop:"ds-uniqueness" ini:"ds-uniqueness" flag:"ds-uniqueness"`
}
