module gitee.com/fatzeng/srf_switch

go 1.18

require (
	gitee.com/fatzeng/srf_switch_basic_components v0.0.7-0.20240627063145-09120b1b3191
	github.com/spf13/cast v1.6.0
)

require gopkg.in/yaml.v2 v2.4.0 // indirect
