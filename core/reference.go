// @Author EthanScriptOn
// @Desc
package core

type TargetReference interface {
	GetTarget() interface{}
}
