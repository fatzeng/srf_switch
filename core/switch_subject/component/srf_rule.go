// @Author EthanScriptOn
// @Desc
package component

import (
	"gitee.com/fatzeng/srf_switch/core/switch_subject"
	"sync"
)

var ruleContainer = &sync.Map{}

func GetRule(switchName string) (*switch_subject.SrfRule, bool) {
	value, ok := ruleContainer.Load(switchName)
	if !ok {
		return nil, false
	}
	rule, ok := value.(*switch_subject.SrfRule)
	return rule, ok
}

func RegisterRule(switchName string, rule *switch_subject.SrfRule) {
	ruleContainer.Store(switchName, rule)
}
