// @Author EthanScriptOn
// @Desc
package statistics

import (
	"context"
	"fmt"
	"strings"
)

type SwitchStatistic struct {
	SwitchName    string
	IsPassThrough bool
	Items         []*SwitchStatisticItem
	OutputFields  []string
	Remark        string
}

type SwitchStatisticItem struct {
	FactorId      interface{}
	IsPassThrough bool
}

func GenerateSwitchStatisticItem(FactorId interface{}, IsPassThrough bool) *SwitchStatisticItem {
	return &SwitchStatisticItem{
		FactorId:      FactorId,
		IsPassThrough: IsPassThrough,
	}
}

func GenerateSwitchStatistic(OutputFields []string) *SwitchStatistic {
	if OutputFields == nil {
		OutputFields = make([]string, 0)
	}
	return &SwitchStatistic{
		Items:        make([]*SwitchStatisticItem, 0),
		OutputFields: OutputFields,
	}
}

func (s *SwitchStatistic) String(ctx context.Context) string {
	outPutBuilder := strings.Builder{}
	outPutBuilder.WriteString(fmt.Sprintf("SwitchName: [%v], IsPassThrough: [%v]", s.SwitchName, s.IsPassThrough))
	itemsContainer := make([]string, 0)
	for _, item := range s.Items {
		itemsContainer = append(itemsContainer, fmt.Sprintf("FactorId: [%v], IsPassThrough: [%v]", item.FactorId, item.IsPassThrough))
	}
	if len(itemsContainer) > 0 {
		outPutBuilder.WriteString("\r\n")
		outPutBuilder.WriteString(fmt.Sprintf(", SwitchStatisticItems: [%v]", itemsContainer))
	}
	values := make(map[interface{}]interface{})
	for _, field := range s.OutputFields {
		value := ctx.Value(field)
		if value != nil {
			values[field] = value
		}
	}
	if len(values) > 0 {
		outPutBuilder.WriteString("\r\n")
		outPutBuilder.WriteString(fmt.Sprintf(", OutputFields: [%v]", values))
	}
	if s.Remark != "" {
		outPutBuilder.WriteString("\r\n")
		outPutBuilder.WriteString(fmt.Sprintf(", Remark: [%v]", s.Remark))
	}
	return outPutBuilder.String()
}
