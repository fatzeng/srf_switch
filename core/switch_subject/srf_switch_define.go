// @Author EthanScriptOn
// @Desc
package switch_subject

const (
	AND = "And_Group"
	OR  = "Or_Group"
)
const (
	STATISTIC            = "Statistic"
	OUT_PUT_FIELDS       = "OutputFields"
	SINGLE_REQUEST_TOKEN = "SingleRequestToken"
)

const IS_OPEN_SWITCH_STATISTIC = "Is_Open_Switch_Statistic"

type SrfRule struct {
	Rule         *SrfRule
	Relationship string
	FactorRules  []*FactorRule
}

type FactorRule struct {
	FactorId   interface{}
	RuleRemark string
}
