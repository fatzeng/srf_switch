// @Author EthanScriptOn
// @Desc
package switch_subject

import (
	"context"
	"gitee.com/fatzeng/srf_switch_basic_components/switch_log"
	"reflect"
	"runtime"
	"strings"
)

type Filter interface {
	Filter(ctx context.Context, rule interface{}) bool
}

type CacheMiddleware interface {
	PutCache(ctx context.Context, cacheItem interface{}, finalRuleState bool)
	IsHitCache(ctx context.Context, cacheItem interface{}) (bool, bool)
}

type FactorActuator func(ctx context.Context, ruleRemark interface{}) (bool, error)

type FactorActuatorRegistry map[interface{}]FactorActuator

func (f FactorActuatorRegistry) Add(faFunc ...FactorActuator) {
	for _, faFun := range faFunc {
		iName := runtime.FuncForPC(reflect.ValueOf(faFun).Pointer()).Name()
		switch_log.Logger().Info("the name of the function being added is: ", iName)
		if strings.Contains(iName, "fun") {
			switch_log.Logger().Warn("please do not use anonymous functions to import a function: ")
		}
		f[iName] = faFun
	}
}

func (f FactorActuatorRegistry) Get(id interface{}) (FactorActuator, bool) {
	actuator, ok := f[id]
	if !ok {
		switch_log.Logger().Warn("unable to find function with ID: ", id)
	}
	return actuator, ok
}
